﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CsharpBits
{
    public class UploadFile
    {
        public UploadFile()
        {
            ContentType = "image/png";
        }
        public string Name { get; set; }
        public string Filename { get; set; }
        public string ContentType { get; set; }
        public Stream Stream { get; set; }
    }
}
