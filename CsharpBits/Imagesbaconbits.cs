﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Collections.Specialized;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CsharpBits
{
    class Imagesbaconbits
    {
        public static string UploadImageUrl(string url)
        {
            var files = new UploadFile[] { };

            var values = new NameValueCollection {
                { "url", url }
            };

            byte[] result = UploadFiles("https://images.baconbits.org/upload.php", files, values);
            string resstr = System.Text.Encoding.UTF8.GetString(result);
            Console.WriteLine(resstr);

            Regex horribleWhyTheFuckAmIParsingJSONWithRegexRegex = new Regex("{\"ImgName\":\"(\\w+\\.\\w+)\"}");
            Match match = horribleWhyTheFuckAmIParsingJSONWithRegexRegex.Match(resstr);
            return "https://images.baconbits.org/images/" + match.Groups[1].ToString();
        }

        public static string UploadImage(string fileName)
        {
            using (var fstream = File.Open(fileName, FileMode.Open))
            {
                var files = new[] 
                {
                    new UploadFile
                    {
                        Name = "ImageUp",
                        Filename = "image.png",
                        ContentType = "image/png",
                        Stream = fstream
                    }
                };

                var values = new NameValueCollection();

                byte[] result = UploadFiles("https://images.baconbits.org/upload.php", files, values);
                string resstr = System.Text.Encoding.UTF8.GetString(result);
                Console.WriteLine(resstr);

                Regex horribleWhyTheFuckAmIParsingJSONWithRegexRegex = new Regex("{\"ImgName\":\"(\\w+\\.\\w+)\"}");
                Match match = horribleWhyTheFuckAmIParsingJSONWithRegexRegex.Match(resstr);
                return "https://images.baconbits.org/images/" + match.Groups[1].ToString();
            }
        }

        // Taken from: http://www.bratched.com/en/component/content/article/69-uploading-multiple-files-with-c.html
        public static byte[] UploadFiles(string address, IEnumerable<UploadFile> files, NameValueCollection values)
        {
            var request = WebRequest.Create(address);
            request.Method = "POST";
            var boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            boundary = "--" + boundary;

            using (var requestStream = request.GetRequestStream())
            {
                // Write the values
                foreach (string name in values.Keys)
                {
                    var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.ASCII.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"{1}{1}", name, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.UTF8.GetBytes(values[name] + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                }

                // Write the files
                foreach (var file in files)
                {
                    var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", file.Name, file.Filename, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType, Environment.NewLine));
                    requestStream.Write(buffer, 0, buffer.Length);
                    file.Stream.CopyTo(requestStream);
                    buffer = Encoding.ASCII.GetBytes(Environment.NewLine);
                    requestStream.Write(buffer, 0, buffer.Length);
                }

                var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);
            }

            using (var response = request.GetResponse())
            using (var responseStream = response.GetResponseStream())
            using (var stream = new MemoryStream())
            {
                responseStream.CopyTo(stream);
                return stream.ToArray();
            }
        }
    }
}
