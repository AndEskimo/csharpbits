﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.Windows.Forms;

namespace CsharpBits
{
    class Ffmpeg
    {
        private string file;
        private string ffmpeg;
        private string imageHost;
        private string tmpDir;
        private int duration;
        private int numShots;
        private XmlConfig conf;

        public Ffmpeg(string filename, string ffmpegPath, ref XmlConfig xc)
        {
            imageHost = null;
            tmpDir = null;
            duration = -1;
            numShots = -1;
            file = filename;
            ffmpeg = ffmpegPath;
            conf = xc;
        }

        public bool FfmpegFound()
        {
            if (File.Exists(ffmpeg))
                return true;
            return false;
        }

        public string TakeScreenshot(int shotnum)
        {
            setGlobals();

            string shot = "";

            if (File.Exists(tmpDir + "screen" + shotnum + ".png"))
            {
                try
                {
                    File.Delete(tmpDir + "screen" + shotnum + ".png");
                }
                catch (Exception) { }
            }

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = ffmpeg;
            p.StartInfo.Arguments = " -ss " + duration * shotnum / (numShots + 1)
            + " -r 1 -i \"" + file + "\" -vframes 1 -f image2 -an -y \""
            + tmpDir + "screen" + shotnum + ".png\"";

            p.Start();
            p.WaitForExit(5000);
            p.Dispose();

            shot = tmpDir + "screen" + shotnum + ".png";
            string tmpImg = shot;
            try
            {
               shot = UploadImage(shot);
            }
            catch (Exception)
            {
                shot = "";
            }
            File.Delete(tmpImg);
            return shot;
        }

        public string[] TakeScreenshots(ref XmlConfig xc)
        {
            setGlobals();

            string[] shots = new string[numShots];

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.FileName = ffmpeg;
            
            for (int i = 1; i <= numShots; i++)
            {
                p.StartInfo.Arguments = " -ss " + duration * i / (numShots + 1)
                + " -r 1 -i \"" + file + "\" -vframes 1 -f image2 -an -y \""
                + tmpDir + "screen" + i + ".png\"";

                p.Start();
                p.WaitForExit();
                shots[i - 1] = tmpDir + "screen" + i + ".png";
            }

            string tmpImg;
            for (int i = 0; i < shots.Length; i++)
            {
                tmpImg = shots[i];
                try
                {
                    shots[i] = UploadImage(shots[i]);
                    
                }
                catch (Exception)
                {
                    shots[i] = "";
                }
                try
                {
                    File.Delete(tmpImg);
                }
                catch (Exception)
                {
                    //Darnit imageshack, why.
                }
            }
            p.Dispose();
            return shots;
        }

        private void setGlobals()
        {
            if (duration == -1)
                duration = getDuration();
            if (numShots == -1)
                numShots = conf.GetIntValue("numscreenshots");
            if (imageHost == null)
                imageHost = conf.GetValue("imagehost");
            if (tmpDir == null)
            {
                tmpDir = Path.GetTempPath() + "\\csharpbits\\";
                if (!Directory.Exists(tmpDir))
                    Directory.CreateDirectory(tmpDir);
            }
        }

        private string UploadImage(string path)
        {
            string ret;
            switch (imageHost.ToLower())
            {
                case "imgur":
                    ret = Imgur.UploadImage(path, conf.GetValue("imgurapikey"));
                    break;
                case "minus":
                    ret = Minus.UploadImage(path);
                    break;
                case "bbimages":
                    ret = Imagesbaconbits.UploadImage(path);
                    break;
                default:
                    ret = Imagesbaconbits.UploadImage(path);
                    break;
            }
            return ret;
        }

        private int getDuration()
        {

            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.FileName = ffmpeg;
            p.StartInfo.Arguments = " -i \"" + file + "\"";
            string output = null;
            StreamReader sr = null;

            try
            {
                p.Start();
                sr = p.StandardError;
                output = sr.ReadToEnd();
                string[] hms;
                Regex re = new Regex("[D|d]uration:.((\\d|:|\\.)*).*\n");
                Match m = re.Match(output);
                if (m.Success)
                {
                    hms = m.Groups[1].Value.Split(new char[] { ':', '.' });
                    duration = int.Parse(hms[0]) * 3600 + int.Parse(hms[1]) * 60 + int.Parse(hms[2]);
                }
                p.WaitForExit(10000);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
            }
            return duration;
        }
    }
}
