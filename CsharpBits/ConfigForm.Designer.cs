﻿namespace CsharpBits
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtDescFormat = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.footnoteResize = new System.Windows.Forms.Label();
            this.cbImdbImage = new System.Windows.Forms.CheckBox();
            this.numScreenshots = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txtImgurApiKey = new System.Windows.Forms.TextBox();
            this.txtFfmpeg = new System.Windows.Forms.TextBox();
            this.txtMediainfo = new System.Windows.Forms.TextBox();
            this.btnFfmpegBrowse = new System.Windows.Forms.Button();
            this.lblImgurApiKey = new System.Windows.Forms.Label();
            this.cmbImageHost = new System.Windows.Forms.ComboBox();
            this.lblFfmpeg = new System.Windows.Forms.Label();
            this.cbNullProxy = new System.Windows.Forms.CheckBox();
            this.cbImgLinks = new System.Windows.Forms.CheckBox();
            this.lblMediaInfo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbGuessTitle = new System.Windows.Forms.CheckBox();
            this.btnMediainfoBrowse = new System.Windows.Forms.Button();
            this.btnImgurApiKey = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.numOpenDelay = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenshots)).BeginInit();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOpenDelay)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(452, 303);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(533, 303);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "\"Executable |*.exe|All Files|*.*\"";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtDescFormat);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(599, 269);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Description";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtDescFormat
            // 
            this.txtDescFormat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescFormat.Location = new System.Drawing.Point(3, 3);
            this.txtDescFormat.Multiline = true;
            this.txtDescFormat.Name = "txtDescFormat";
            this.txtDescFormat.Size = new System.Drawing.Size(593, 260);
            this.txtDescFormat.TabIndex = 0;
            this.txtDescFormat.TextChanged += new System.EventHandler(this.txtDescFormat_TextChanged_1);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.numOpenDelay);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.footnoteResize);
            this.tabPage1.Controls.Add(this.cbImdbImage);
            this.tabPage1.Controls.Add(this.numScreenshots);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtImgurApiKey);
            this.tabPage1.Controls.Add(this.txtFfmpeg);
            this.tabPage1.Controls.Add(this.txtMediainfo);
            this.tabPage1.Controls.Add(this.btnFfmpegBrowse);
            this.tabPage1.Controls.Add(this.lblImgurApiKey);
            this.tabPage1.Controls.Add(this.cmbImageHost);
            this.tabPage1.Controls.Add(this.lblFfmpeg);
            this.tabPage1.Controls.Add(this.cbNullProxy);
            this.tabPage1.Controls.Add(this.cbImgLinks);
            this.tabPage1.Controls.Add(this.lblMediaInfo);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.cbGuessTitle);
            this.tabPage1.Controls.Add(this.btnMediainfoBrowse);
            this.tabPage1.Controls.Add(this.btnImgurApiKey);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(599, 269);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Basic";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // footnoteResize
            // 
            this.footnoteResize.AutoSize = true;
            this.footnoteResize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.footnoteResize.Location = new System.Drawing.Point(3, 251);
            this.footnoteResize.Name = "footnoteResize";
            this.footnoteResize.Size = new System.Drawing.Size(277, 13);
            this.footnoteResize.TabIndex = 125;
            this.footnoteResize.Text = "* Note: this image host resizes or transcodes large images";
            this.footnoteResize.Visible = false;
            // 
            // cbImdbImage
            // 
            this.cbImdbImage.AutoSize = true;
            this.cbImdbImage.Checked = true;
            this.cbImdbImage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbImdbImage.Location = new System.Drawing.Point(6, 162);
            this.cbImdbImage.Name = "cbImdbImage";
            this.cbImdbImage.Size = new System.Drawing.Size(110, 17);
            this.cbImdbImage.TabIndex = 124;
            this.cbImdbImage.Text = "Grab cover image";
            this.cbImdbImage.UseVisualStyleBackColor = true;
            this.cbImdbImage.CheckedChanged += new System.EventHandler(this.cbImdbImage_CheckedChanged);
            // 
            // numScreenshots
            // 
            this.numScreenshots.Location = new System.Drawing.Point(166, 58);
            this.numScreenshots.Name = "numScreenshots";
            this.numScreenshots.Size = new System.Drawing.Size(32, 20);
            this.numScreenshots.TabIndex = 4;
            this.numScreenshots.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 15);
            this.label2.TabIndex = 38;
            this.label2.Text = "Number of Screenshots";
            // 
            // txtImgurApiKey
            // 
            this.txtImgurApiKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImgurApiKey.Location = new System.Drawing.Point(317, 135);
            this.txtImgurApiKey.Name = "txtImgurApiKey";
            this.txtImgurApiKey.Size = new System.Drawing.Size(167, 20);
            this.txtImgurApiKey.TabIndex = 1;
            this.txtImgurApiKey.TabStop = false;
            this.txtImgurApiKey.Visible = false;
            this.txtImgurApiKey.TextChanged += new System.EventHandler(this.txtImgur_TextChanged);
            // 
            // txtFfmpeg
            // 
            this.txtFfmpeg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFfmpeg.Location = new System.Drawing.Point(143, 7);
            this.txtFfmpeg.Name = "txtFfmpeg";
            this.txtFfmpeg.Size = new System.Drawing.Size(341, 20);
            this.txtFfmpeg.TabIndex = 3;
            this.txtFfmpeg.TabStop = false;
            this.txtFfmpeg.TextChanged += new System.EventHandler(this.txtFfmpeg_TextChanged);
            // 
            // txtMediainfo
            // 
            this.txtMediainfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMediainfo.Location = new System.Drawing.Point(143, 35);
            this.txtMediainfo.Name = "txtMediainfo";
            this.txtMediainfo.Size = new System.Drawing.Size(341, 20);
            this.txtMediainfo.TabIndex = 123;
            this.txtMediainfo.TabStop = false;
            this.txtMediainfo.TextChanged += new System.EventHandler(this.txtMediainfo_TextChanged);
            // 
            // btnFfmpegBrowse
            // 
            this.btnFfmpegBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFfmpegBrowse.Location = new System.Drawing.Point(490, 8);
            this.btnFfmpegBrowse.Name = "btnFfmpegBrowse";
            this.btnFfmpegBrowse.Size = new System.Drawing.Size(97, 20);
            this.btnFfmpegBrowse.TabIndex = 2;
            this.btnFfmpegBrowse.Text = "Browse";
            this.btnFfmpegBrowse.UseVisualStyleBackColor = true;
            this.btnFfmpegBrowse.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblImgurApiKey
            // 
            this.lblImgurApiKey.AutoSize = true;
            this.lblImgurApiKey.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgurApiKey.Location = new System.Drawing.Point(216, 136);
            this.lblImgurApiKey.Name = "lblImgurApiKey";
            this.lblImgurApiKey.Size = new System.Drawing.Size(95, 15);
            this.lblImgurApiKey.TabIndex = 23;
            this.lblImgurApiKey.Text = "Imgur Api Key";
            this.lblImgurApiKey.Visible = false;
            // 
            // cmbImageHost
            // 
            this.cmbImageHost.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbImageHost.Items.AddRange(new object[] {
            "bBImages",
            "Minus",
            "Imgur"});
            this.cmbImageHost.Location = new System.Drawing.Point(89, 135);
            this.cmbImageHost.Name = "cmbImageHost";
            this.cmbImageHost.Size = new System.Drawing.Size(121, 21);
            this.cmbImageHost.TabIndex = 11;
            this.cmbImageHost.SelectedIndexChanged += new System.EventHandler(this.cmbImageHost_SelectedIndexChanged);
            // 
            // lblFfmpeg
            // 
            this.lblFfmpeg.AutoSize = true;
            this.lblFfmpeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFfmpeg.Location = new System.Drawing.Point(3, 10);
            this.lblFfmpeg.Name = "lblFfmpeg";
            this.lblFfmpeg.Size = new System.Drawing.Size(88, 15);
            this.lblFfmpeg.TabIndex = 22;
            this.lblFfmpeg.Text = "Ffmpeg Path";
            // 
            // cbNullProxy
            // 
            this.cbNullProxy.AutoSize = true;
            this.cbNullProxy.Checked = true;
            this.cbNullProxy.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbNullProxy.Location = new System.Drawing.Point(193, 83);
            this.cbNullProxy.Name = "cbNullProxy";
            this.cbNullProxy.Size = new System.Drawing.Size(95, 17);
            this.cbNullProxy.TabIndex = 9;
            this.cbNullProxy.Text = "Use null proxy.";
            this.cbNullProxy.UseVisualStyleBackColor = true;
            this.cbNullProxy.CheckedChanged += new System.EventHandler(this.cbNullProxy_CheckedChanged);
            // 
            // cbImgLinks
            // 
            this.cbImgLinks.AutoSize = true;
            this.cbImgLinks.Checked = true;
            this.cbImgLinks.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbImgLinks.Location = new System.Drawing.Point(181, 107);
            this.cbImgLinks.Name = "cbImgLinks";
            this.cbImgLinks.Size = new System.Drawing.Size(130, 17);
            this.cbImgLinks.TabIndex = 10;
            this.cbImgLinks.Text = "Open screenshots too";
            this.cbImgLinks.UseVisualStyleBackColor = true;
            this.cbImgLinks.CheckedChanged += new System.EventHandler(this.cbImgLinks_CheckedChanged);
            // 
            // lblMediaInfo
            // 
            this.lblMediaInfo.AutoSize = true;
            this.lblMediaInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMediaInfo.Location = new System.Drawing.Point(3, 36);
            this.lblMediaInfo.Name = "lblMediaInfo";
            this.lblMediaInfo.Size = new System.Drawing.Size(129, 15);
            this.lblMediaInfo.TabIndex = 28;
            this.lblMediaInfo.Text = "MediaInfo CLI Path";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 41;
            this.label3.Text = "Image Host";
            // 
            // cbGuessTitle
            // 
            this.cbGuessTitle.AutoSize = true;
            this.cbGuessTitle.Location = new System.Drawing.Point(6, 83);
            this.cbGuessTitle.Name = "cbGuessTitle";
            this.cbGuessTitle.Size = new System.Drawing.Size(181, 17);
            this.cbGuessTitle.TabIndex = 8;
            this.cbGuessTitle.Text = "Automatically Guess Movie Titles";
            this.cbGuessTitle.UseVisualStyleBackColor = true;
            this.cbGuessTitle.CheckedChanged += new System.EventHandler(this.cbGuessTitle_CheckedChanged);
            // 
            // btnMediainfoBrowse
            // 
            this.btnMediainfoBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMediainfoBrowse.Location = new System.Drawing.Point(490, 34);
            this.btnMediainfoBrowse.Name = "btnMediainfoBrowse";
            this.btnMediainfoBrowse.Size = new System.Drawing.Size(97, 23);
            this.btnMediainfoBrowse.TabIndex = 3;
            this.btnMediainfoBrowse.Text = "Browse";
            this.btnMediainfoBrowse.UseVisualStyleBackColor = true;
            this.btnMediainfoBrowse.Click += new System.EventHandler(this.button5_Click);
            // 
            // btnImgurApiKey
            // 
            this.btnImgurApiKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImgurApiKey.Location = new System.Drawing.Point(490, 133);
            this.btnImgurApiKey.Name = "btnImgurApiKey";
            this.btnImgurApiKey.Size = new System.Drawing.Size(97, 23);
            this.btnImgurApiKey.TabIndex = 2;
            this.btnImgurApiKey.TabStop = false;
            this.btnImgurApiKey.Text = "Get a New Key";
            this.btnImgurApiKey.UseVisualStyleBackColor = true;
            this.btnImgurApiKey.Visible = false;
            this.btnImgurApiKey.Click += new System.EventHandler(this.button7_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(607, 295);
            this.tabControl1.TabIndex = 1;
            // 
            // numOpenDelay
            // 
            this.numOpenDelay.DecimalPlaces = 2;
            this.numOpenDelay.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numOpenDelay.Location = new System.Drawing.Point(121, 106);
            this.numOpenDelay.Name = "numOpenDelay";
            this.numOpenDelay.Size = new System.Drawing.Size(48, 20);
            this.numOpenDelay.TabIndex = 126;
            this.numOpenDelay.ValueChanged += new System.EventHandler(this.numOpenDelay_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 15);
            this.label1.TabIndex = 127;
            this.label1.Text = "Link Open Delay";
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(608, 326);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(400, 285);
            this.Name = "ConfigForm";
            this.Text = "CsharpBits Configuration";
            this.Load += new System.EventHandler(this.ConfigForm_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numScreenshots)).EndInit();
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numOpenDelay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtDescFormat;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.NumericUpDown numScreenshots;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtImgurApiKey;
        private System.Windows.Forms.TextBox txtFfmpeg;
        private System.Windows.Forms.TextBox txtMediainfo;
        private System.Windows.Forms.Button btnFfmpegBrowse;
        private System.Windows.Forms.Label lblImgurApiKey;
        private System.Windows.Forms.ComboBox cmbImageHost;
        private System.Windows.Forms.Label lblFfmpeg;
        private System.Windows.Forms.CheckBox cbNullProxy;
        private System.Windows.Forms.CheckBox cbImgLinks;
        private System.Windows.Forms.Label lblMediaInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbGuessTitle;
        private System.Windows.Forms.Button btnMediainfoBrowse;
        private System.Windows.Forms.Button btnImgurApiKey;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox cbImdbImage;
        private System.Windows.Forms.Label footnoteResize;
        private System.Windows.Forms.NumericUpDown numOpenDelay;
        private System.Windows.Forms.Label label1;

    }
}